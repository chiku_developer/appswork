var _isDestroyFlg = false;

/*
 * アイテム表示処理(成功)
 */
function diplayItemsSuccess(data, dataType) {

	if (!data || data.length == 0) {
		if (isMessageClosed()) {
			$("#message-area")
					.html(
							'<div id="message" class="alert alert-info" role="alert"><button id="btnMessageClose" type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><p id="message-text" class="text-center">おすすめ小説.comは厳選したおすすめ小説の紹介サービスです。</p></div>');
		}

		$("#message").addClass("alert-warning");
		$("#message").removeClass("alert-info");
		$("#message-text").html("検索結果がありません");
		callFooterFixed();
		jQuery.unblockUI();
		return;
	}

	if (!isMessageClosed()) {
		$("#message-text")
				.html(
						"おすすめ小説.comは厳選したおすすめ小説の紹介サービスです。");
		$("#message").addClass("alert-info");
		$("#message").removeClass("alert-warning");
	} else {
		$("#message-area").html("");
	}

	for (var i = 0; i < data.length; i++) {
		var divString = '';

		divString += '<div class="thumb col-md-3 col-sm-4 col-xs-12 nopadding">';
		divString += '  <a class="item_link" href="';
		divString += data[i].url;
		divString += '  " target="_blank">';
		divString += '  <div class="item">';
		divString += '    <div class="item_main">';
		divString += '      <img class="pin_image img-responsive" src="';
		divString += data[i].img_path;
		divString += '      ">';
		divString += '      <div class="item_text_header">';
		if (data[i].title.length > 18) {
			divString += '      <a class="item_text_header" style="font-size: 12px;" href="';
		} else {
			divString += '      <a class="item_text_header" href="';
		}
		divString += data[i].url;
		divString += '/?_encoding=UTF8&camp=247&creative=1211&linkCode=ur2&tag=';
		divString += data[i].amazon_id;
		divString += '" target="_blank">';
		divString += data[i].title;
		divString += '      </a>';
		divString += '<img src="http://ir-jp.amazon-adsystem.com/e/ir?t=';
		divString += data[i].amazon_id;
		divString += '&l=ur2&o=9" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />';
		divString += '      </div>';

		divString += '      <div class="item_content">';
		if (data[i].content) {
			divString += data[i].content.replace(/[\n\r]/g,"");
		}
		divString += '      </div>';

		divString += '      <div style="float:right; width:100px; margin-top:3px;"><a href="https://twitter.com/share" class="twitter-share-button" data-text="';
		divString += data[i].title + " [おすすめ小説.com]";
		divString += '">Tweet</a></div>';

		divString += '      <div style="clear:both;"></div>';

		divString += '      <hr class="item_hr" style="margin-top:2px !important;" />';
		
		divString += '    </div>';
		
		divString += '    <div class="item_bottom">';
		
		divString += '    <div class="item_bottom_inner">';
		
		divString += '      <div style="display: inline-block;">';
		if (data[i].favorite_flg && data[i].favorite_flg == 1) {
			divString += '      <a type="button" class="btn btn-warning btn-sm" role="button" id="favorite-btn';
		} else {
			divString += '      <a type="button" class="btn btn-primary btn-sm" role="button" id="favorite-btn';
		}
		divString += data[i].item_id;
		divString += '" onclick="doFavorite(';
		divString += data[i].item_id;
		if (data[i].favorite_flg && data[i].favorite_flg == 1) {
			divString += '      )") ><span class="glyphicon glyphicon-star" id="favorite-star';
		} else {
			divString += '      )") ><span class="glyphicon glyphicon-star-empty" id="favorite-star';
		}
		divString += data[i].item_id;
		divString += '      "></span> <span id="favorite';
		divString += data[i].item_id;
		divString += '      ">お気に入り！ (';
		divString += " " + data[i].favorite_count + " ";
		divString += '      )</span></a>';
		
		divString += '      </div>';

		divString += '      <div style="display: inline-block;margin-left:10px;">';
		divString += '      <a href="';
		divString += data[i].url.replace("http://www.amazon.co.jp/dp/", "http://www.amazon.co.jp/product-reviews/");
		divString += '?_encoding=UTF8&camp=247&creative=1211&linkCode=ur2&tag=';
		divString += data[i].amazon_id;
		divString += '" class="btn btn-default btn-sm" target="_blank">レビュー</a></div>';
		divString += '      <div style="clear:both;"></div>';
		divString += '    </div>';
		divString += '    </div>';
		divString += '  </div>';
		divString += '  <div style="clear:both;"></div>';
		divString += '  </div> </a>';
		divString += '</div>';

		$("#thumbnails").append(divString);
	}

	jQuery.unblockUI();

	callFooterFixed();

	setTimeout("doBackground()", 100);
}

function doBackground() {
	var $container = $('#thumbnails');
	$container.imagesLoaded(function() {
		$container.masonry({
			isAnimated : true,
			"isFitWidth": false,
			columnWidth : '.thumb',
			itemSelector : '.thumb'
		});
	});
	_isDestroyFlg = true;

	try {
		twttr.widgets.load();
	} catch (e) {
		
	}
}

/*
 * アイテム表示処理(エラー)
 */
function diplayItemsError(XMLHttpRequest, textStatus, errorThrown) {
	jQuery.unblockUI();
}

/*
 * アイテム表示処理(完了)
 */
function diplayItemsComplete(XMLHttpRequest, textStatus) {
	jQuery.unblockUI();
}

/*
 * アイテム検索処理
 */
function searchItems() {

//	// ローディング表示
//	displayLoding();
//
	var $container = $('#thumbnails');
	$container.masonry('destroy');
	if (_isDestroyFlg) {
		var $container = $('#thumbnails');
		$container.masonry('destroy');
	}
//
//	// 検索結果クリア
//	$("#thumbnails").empty();
//
//	// 検索条件取得
//	var jsondata = getSearchWhere();
//
//	$.ajax({
//		type : "GET",
//		dataType : "json",
//		scriptCharset : "utf-8",
//		contentType : "application/json; charset=utf-8",
//		cache : false,
//		data : {
//			entity : JSON.stringify(jsondata)
//		},
//		url : "items/show",
//		async : true,
//		crossDomain : false,
//		success : diplayItemsSuccess,
//		error : diplayItemsError,
//		complete : diplayItemsComplete,
//		timeout : 10000
//	});
}