/* 

	index.js
	メイン処理
	
 */

var _cookieName = "uniqueId"; // クッキー,LocalStorageの名前

var _uniqueId = "";
var _searchWhere;

/*
 * ローディング表示処理
 */
function displayLoding() {
	$.blockUI({
		message : jQuery('div#loading'),
		fadeIn : 200,
		fadeOut : 200,
		overlayCSS : {
			backgroundColor : '#aaa',
			opacity : 0.6,
			cursor : 'wait'
		},
		css : {
			padding : '30px 0 0 0',
			margin : 0,
			height : '100px',
			width : '100px',
			top : (jQuery(window).height() - 100) / 2 + 'px',
			left : (jQuery(window).width() - 100) / 2 + 'px',
			border : '0px solid #aaa',
			backgroundColor : 'clearcolor',
		}
	});
	setTimeout(jQuery.unblockUI, 5000);
}

/*
 * ローカルストレージ判定
 */
function isLocalStorage() {
	try {
		window.localStorage.setItem("dummy", "dummy");
		return true;
	} catch (e) {
		return false;
	}
}

/*
 * footerFixedのCall
 */
function callFooterFixed() {
	footerFixed();
	setTimeout(footerFixed, 100);
}

/*
 * ランダム文字列生成
 */
function getRandobet(n, b) {
	b = b || '';
	var a = 'abcdefghijklmnopqrstuvwxyz' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
			+ '0123456789' + b;
	a = a.split('');
	var s = '';
	for (var i = 0; i < n; i++) {
		s += a[Math.floor(Math.random() * a.length)];
	}
	return s;
}

/*
 * ユーザーユニークID取得
 */
function getUserUniquerId() {
	var uniqueId;

	if (isLocalStorage()) {
		uniqueId = localStorage.getItem("uniqueId");
	} else {
		uniqueId = $.cookie("uniqueId");
	}

	if (!uniqueId) {

		uniqueId = getRandobet(20, '-_!');
		if (isLocalStorage()) {
			localStorage.setItem("uniqueId", uniqueId);
		} else {
			$.cookie("uniqueId", uniqueId);
		}
	}

	return uniqueId;
}

/*
 * 通知クローズセット
 */
function setNoiceClosed(flg) {
	if (isLocalStorage()) {
		localStorage.setItem("isMessageClosed", flg);
	} else {
		$.cookie("isMessageClosed", flg);
	}
}

/*
 * 通知クローズ判定
 */
function isMessageClosed() {
	var flg = null;
	if (isLocalStorage()) {
		flg = localStorage.getItem("isMessageClosed");
	} else {
		flg = $.cookie("isMessageClosed");
	}

	if (flg && flg == "1") {
		return true;
	}
	return false;
}

/*
 * 検索条件取得
 */
function getSearchWhere() {
	var jsondata = new Object();

	jsondata.uniquerId = _uniqueId;
	if (isLocalStorage()) {
		jsondata.categoryId = localStorage.getItem("categoryId");
		jsondata.keyword = localStorage.getItem("keyword");
		jsondata.orderby = localStorage.getItem("orderby");
		jsondata.favoriteFlg = localStorage.getItem("favoriteFlg");
	} else {
		jsondata.categoryId = $.cookie("categoryId");
		jsondata.keyword = $.cookie("keyword");
		jsondata.orderby = $.cookie("orderby");
		jsondata.favoriteFlg = $.cookie("favoriteFlg");
	}

	return jsondata;
}

/*
 * 検索条件保存
 */
function saveSearchWhere(searchWhere) {
	var jsondata = new Object();

	if (isLocalStorage()) {
		if (searchWhere.categoryId || searchWhere.categoryId == 0) {
			localStorage.setItem("categoryId", searchWhere.categoryId);
		}
		if (searchWhere.keyword) {
			if (searchWhere.keyword == " ") {
				searchWhere.keyword = "";
			}
			localStorage.setItem("keyword", searchWhere.keyword);
		}
		if (searchWhere.orderby || searchWhere.orderby == 0) {
			localStorage.setItem("orderby", searchWhere.orderby);
		}
		if (searchWhere.favoriteFlg || searchWhere.favoriteFlg == 0) {
			localStorage.setItem("favoriteFlg", searchWhere.favoriteFlg);
		}
	} else {
		if (searchWhere.categoryId || searchWhere.categoryId == 0) {
			$.cookie("categoryId", searchWhere.categoryId);
		}
		if (searchWhere.keyword) {
			$.cookie("keyword", searchWhere.keyword);
		}
		if (searchWhere.orderby || searchWhere.orderby == 0) {
			$.cookie("orderby", searchWhere.orderby);
		}
		if (searchWhere.favoriteFlg || searchWhere.favoriteFlg == 0) {
			$.cookie("favoriteFlg", searchWhere.favoriteFlg);
		}
	}
}

/*
 * 検索ボタンクリック時
 */
function onClick_search() {

	// キーワード検索条件保存
	var searchWhere = new Object();
	searchWhere.keyword = $("#keyword").val();
	if (searchWhere.keyword == "") {
		searchWhere.keyword = " ";
	}
	saveSearchWhere(searchWhere);

	// 検索処理
	searchItems();

	return false;
}

/*
 * 検索初期表示
 */
function initSearchDisplay() {
	var searchWhere = getSearchWhere();

	var name = "";
	if (searchWhere.categoryId || searchWhere.categoryId != "0") {

		switch (searchWhere.categoryId) {
		case "1":
			name = "ミステリー";
			break;
		case "2":
			name = "SF";
			break;
		case "3":
			name = "ファンタジー";
			break;
		case "4":
			name = "歴史・時代";
			break;
		case "5":
			name = "現代・純文学";
			break;
		case "6":
			name = "ホラー";
			break;
		case "7":
			name = "その他";
			break;
		}

		if (name) {
			$("#dropdown-toggle-category").html(
					name + ' <span class="caret"></span>');
		}
	}

	if (searchWhere.keyword) {
		if (searchWhere.keyword == " ") {
			searchWhere.keyword = "";
		}
		$("#keyword").val(searchWhere.keyword);
	}

	if (searchWhere.orderby || searchWhere.orderby != "0") {

		switch (searchWhere.orderby) {
		case "1":
			name = "新着順";
			break;
		case "2":
			name = "古い順";
			break;
		case "3":
			name = "お気に入り数順";
			break;
		}

		if (name) {
			$("#dropdown-toggle-order").html(
					name + ' <span class="caret"></span>');
		}
	}

	if (searchWhere.favoriteFlg || searchWhere.favoriteFlg == "0") {
		if (searchWhere.favoriteFlg == "0") {
			$('#favorite-toggle').bootstrapToggle('off');

		} else {
			$('#favorite-toggle').bootstrapToggle('on');

		}
	}
}

function openCollapseArea() {
	if (!$('#area-box').attr('aria-expanded') || $('#area-box').attr('aria-expanded') == 'false') {
		$("#btnProfileArea").html("閉じる");
	} else {
		$("#btnProfileArea").html("選択");
	}
	$('#area-box').collapse('toggle');
}

function openCollapseCategory() {
	if (!$('#category-box').attr('aria-expanded') || $('#category-box').attr('aria-expanded') == 'false') {
		$("#btnProfileWorkCategory").html("閉じる");
	} else {
		$("#btnProfileWorkCategory").html("選択");
	}
	$('#category-box').collapse('toggle');
}

function openCollapseEntry() {
	if (!$('#entry-box').attr('aria-expanded') || $('#entry-box').attr('aria-expanded') == 'false') {
		$("#btnEntry").html("<i class='glyphicon glyphicon-arrow-up'></i>&nbsp;提案を閉じる");
	} else {
		$("#btnEntry").html("<i class='glyphicon glyphicon-arrow-down'></i>&nbsp;提案する");
	}
	$('#entry-box').collapse('toggle');
}

function openCollapseEntryAdd() {
	if (!$('#entry-box').attr('aria-expanded') || $('#entry-box').attr('aria-expanded') == 'false') {
		$("#btnEntry").html("<i class='glyphicon glyphicon-arrow-up'></i>&nbsp;提案を閉じる");
	} else {
		$("#btnEntry").html("<i class='glyphicon glyphicon-arrow-down'></i>&nbsp;追加で提案する");
	}
	$('#entry-box').collapse('toggle');
}

/*
 * 初期処理
 */
function initProc() {

//	$("#modal-profile").modal("show");

	$('#datetimepicker1').datetimepicker({
		format : 'YYYY-MM-DD',
		language: 'ja',
	});

	return;
	
	// ユーザーユニークID取得
	_uniqueId = getUserUniquerId();

	// 初期表示では通知は表示されるようにする
	setNoiceClosed("0");

	// 検索初期表示
	initSearchDisplay();

	// 検索処理
	searchItems();

	// アイテムマウスオーバー時背景色
	$(".item").mouseover(function() {
		this.style.backgroundColor = "#F2F2F2";
		this.style.opacity = "0.85";
	});

	// アイテムマウスアウト時背景色
	$(".item").mouseout(function() {
		this.style.backgroundColor = "#F2F2F2";
		this.style.opacity = "1.0";
	});

	$('#favorite-toggle').change(function() {
		// 検索条件を保存し、再検索する
		var searchWhere = new Object();
		if ($(this).prop('checked')) {
			searchWhere.favoriteFlg = 1;
		} else {
			searchWhere.favoriteFlg = 0;
		}
		saveSearchWhere(searchWhere);
		searchItems();
	});

	// 
	$("#btnMessageClose").click(function() {
		setNoiceClosed("1");
		callFooterFixed();
	});

	// ドロップダウン変更時処理
	$(".dropdown-menu li a")
			.click(
					function() {
						var searchWhere = new Object();

						// 変更表示を反映
						$(this)
								.parents('.dropdown')
								.find('.dropdown-toggle')
								.html(
										$(this).text()
												+ ' <span class="caret"></span>');

						// 変更を反映
						if ($(this)[0].className.split(" ")[0]
								&& $(this)[0].className.split(" ")[0] == 'dropdown-sort') {
							$(this).parents('.dropdown').find(
									'input[name="dropdown-sort-value"]').val(
									$(this).attr("data-value"));

							searchWhere.orderby = $(this).attr("data-value");

						} else {
							$(this).parents('.dropdown').find(
									'input[name="dropdown-category-value"]')
									.val($(this).attr("data-value"));

							searchWhere.categoryId = $(this).attr("data-value");

						}

						// 検索条件を保存し、再検索する
						saveSearchWhere(searchWhere);
						searchItems();

					});
}

/*
 * 初期処理
 */
$(document).ready(function() {
	try {
		// 初期処理実行
		initProc();

	} catch (e) {
	}
});