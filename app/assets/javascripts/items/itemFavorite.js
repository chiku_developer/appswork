/*
 * お気に入り処理(成功)
 */
function doItemFavoriteSuccess(data, dataType) {

	if (!data || !data.success) {
		if (isMessageClosed()) {
			$("#message-area")
					.html(
							'<div id="message" class="alert alert-info" role="alert"><button id="btnMessageClose" type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><p id="message-text" class="text-center">アダルトハンターズは無料エロ動画検索Webアプリです。<br />ログイン不要でお気に入りや検索を保持しておく事が可能です。</p></div>');
		}

		$("#message").addClass("alert-warning");
		$("#message").removeClass("alert-info");
		$("#message-text").html("お気に入り処理に失敗しました");

		return;
	}

	$("#favorite-btn" + data.itemId).html("お気に入り！ ( " + data.favoriteCount + " )");

	if (data.favoriteFlg) {
		$("#favorite-btn" + data.itemId).addClass("btn-warning");
		$("#favorite-btn" + data.itemId).removeClass("btn-primary");
		$("#favorite-star" + data.itemId).addClass("glyphicon-star");
		$("#favorite-star" + data.itemId).removeClass("glyphicon-star-empty");
	} else {
		$("#favorite-btn" + data.itemId).addClass("btn-primary");
		$("#favorite-btn" + data.itemId).removeClass("btn-warning");
		$("#favorite-star" + data.itemId).addClass("glyphicon-star-empty");
		$("#favorite-star" + data.itemId).removeClass("glyphicon-star");
	}
}

/*
 * お気に入り処理(エラー)
 */
function doItemFavoriteError(XMLHttpRequest, textStatus, errorThrown) {
	jQuery.unblockUI();
}

/*
 * お気に入り処理(完了)
 */
function doItemFavoriteComplete(XMLHttpRequest, textStatus) {
	jQuery.unblockUI();
}

/*
 * お気に入り処理
 */
function doFavorite(itemId) {

	var jsondata = new Object();
	jsondata.uniquerId = _uniqueId;
	jsondata.itemId = itemId;

	$.ajax({
		type : "GET",
		dataType : "json",
		scriptCharset : "utf-8",
		contentType : "application/json; charset=utf-8",
		cache : false,
		data : {
			entity : JSON.stringify(jsondata)
		},
		url : "items/favorite",
		async : true,
		crossDomain : false,
		success : doItemFavoriteSuccess,
		error : doItemFavoriteError,
		complete : doItemFavoriteComplete,
		timeout : 10000
	});
}